package com.example.explisitandimplisitintent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ShowActivity extends AppCompatActivity {
    TextView txtshow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        txtshow = (TextView) findViewById(R.id.txt_show);
        Bundle bundle   = getIntent().getExtras();
        txtshow.setText(bundle.getString("nama"));
    }
}
