package com.example.explisitandimplisitintent;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btn_ex, btn_in;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_ex  = (Button) findViewById(R.id.btn_explisit);
        btn_in  = (Button) findViewById(R.id.btn_implisit);

        btn_ex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ExplisitIntent.class);
                startActivity(intent);
            }
        });


        btn_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent   = getPackageManager().getLaunchIntentForPackage("com.example.calculator");
                if (intent !=null)
                    startActivity(intent);
            }
        });
    }
}
