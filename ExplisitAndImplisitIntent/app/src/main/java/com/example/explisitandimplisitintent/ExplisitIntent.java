package com.example.explisitandimplisitintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ExplisitIntent extends AppCompatActivity {

    EditText editText;
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explisit_intent);

        editText    = (EditText) findViewById(R.id.editetxt);
        btnSend     = (Button) findViewById(R.id.btn_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent   = new Intent(ExplisitIntent.this, ShowActivity.class);
                intent.putExtra("nama", editText.getText().toString());
                startActivity(intent);
            }
        });
    }
}
